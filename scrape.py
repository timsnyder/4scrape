from bs4 import BeautifulSoup
import requests
import sys
import os
import math
import click

def scrape(num_imgs = 100, keywords = "search"):
	"""Scrapes images from 4walled.org"""

	imgs_per_request = 30
	offset = 30
	num_requests = math.ceil(num_imgs/imgs_per_request)
	response = ''
	img_urls = []
	temp = ''
	BASE_URL = 'http://4walled.cc'
	BASE_IMG_URL = 'http://4walled.cc/src/'

    #todo: format keywords to be url friendly
	url = BASE_URL + '/results.php?search=' + keywords + '&board=&width_aspect=&tags=&searchstyle=larger&sfw=0&offset=' + str(offset)

	i = 0
	
	#iterate through requests, api returns 30 imgs at a time
	while i < num_requests:
		print("Getting batch %i of image URLs..." % i)
		response = requests.get(url)
		soup = BeautifulSoup(response.content)
		img_tags = soup.find_all('img')
		#get data from dom, build list of img urls
		for x in img_tags:
			temp = x.get('src').split('/')
			img_urls.append(BASE_IMG_URL + str(temp[-2]) + '/' + str(temp[-1]))
		i = i + 1
		url = BASE_URL + '/results.php?search=' + keywords + '&board=&width_aspect=&tags=&searchstyle=larger&sfw=0&offset=' + str(offset * i)


	#download imgs
	i = 0
	print("Downloading images. This may take a while...")
	with click.progressbar(img_urls) as urls:
		for i in urls:
			r = requests.get(i)
			filename = i.split("/")[-1]
			f = open("scraped_imgs/%s" % filename, 'w+b')
			f.write(r.content)
			f.close()

	#show finished msg
	print('Done!')

#keywords = input("Keywords to search on: ")
num_imgs = input("Number of images to download: ")
num_imgs = int(num_imgs)
scrape(num_imgs)